var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array").ObservableArray;
var Label = require("ui/label").Label;
var bluetooth = require("nativescript-bluetooth");
var LabelModule = require("ui/label");

function getMessage(counter) {
    if (counter <= 0) {
        return "Hoorraaay! You unlocked the NativeScript clicker achievement!";
    } else {
        return counter + " taps left";
    }
}

//var title = new Observable({
  //mainStatement: "Press button to do a 5 second scan for beacons",
//});

var mainStatement = "Press button to do a 5 second scan for beacons";

var label = new LabelModule.Label();
label.textWrap = true;
label.text = "Press button to do a 5 second scan for beacons";

function createViewModel() {
    var viewModel = new Observable();



    //var mainStatement = "Press button to do a 5 second scan for beacons";
    //viewModel.counter = 42;
    viewModel.message = getMessage(viewModel.counter);

    viewModel.label = label.text;


    viewModel.beacons =  new ObservableArray();
    //beacons.push("Numero Uno");
    //beacons.push("Numero Dos");
    //beacons.push("Numero Tres");

    viewModel.onTap = function()
    {
        this.counter--;
        this.set("message", getMessage(this.counter));
    }

    viewModel.isBluetoothOn = function()
    {
      bluetooth.isBluetoothEnabled().then(function(enabled) {
        console.log("Enabled? " + enabled);
        });
    }

    viewModel.beginScan = function()
    {
      //Android 6 and up requires checking for coarse location permission
      //Check for coarse location permission
      bluetooth.hasCoarseLocationPermission().then(
        function(){
          //Found permission, now scan.
          bluetooth.startScanning({
            serviceUUIDs: [],
            seconds: 4,
            onDiscovered: function (peripheral) {
              viewModel.beacons.push(peripheral);
              console.log("Periperhal found with UUID: " + peripheral.UUID);
           }
          }).then(function() {
            console.log("scanning complete");
          }, function (err) {
            console.log("error while scanning: " + err);
          });
          //Add found signals to beacons.
        },
        function(){
          //Did not find permission
          bluetooth.requestCoarseLocationPermission();
        }
      );

    }

    return viewModel;
}

exports.createViewModel = createViewModel;
